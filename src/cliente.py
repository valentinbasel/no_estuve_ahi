#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################


import socket
import serial
import time

##  Variables globales
a = list(range(31, 0, -1))
arduino = serial.Serial('/dev/ttyACM0', 9600)

## funciones
def Measure():
    distance = arduino.readline()
    return int(distance)

## loop principal
while True:
    s = Measure()
    if s <30:
        mySocket = socket.socket(family=socket.AF_INET, 
                                  type=socket.SOCK_STREAM, 
                                  proto=0, 
                                  fileno=None)
        hostname = 'localhost'
        portno = 9999
        mySocket.connect((hostname, portno))
        print("p: ",a[s])
        msg = str(a[s])
        mySocket.send(msg.encode())
        msg_in = mySocket.recv(1024).decode()
        print("##",msg_in)
        mySocket.close()
    else:
        mySocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM, proto=0, fileno=None)
        hostname = 'localhost'
        portno = 9999
        mySocket.connect((hostname, portno))
        msg = str(1)
        mySocket.send(msg.encode())
        msg_in = mySocket.recv(1024).decode()
        mySocket.close()
