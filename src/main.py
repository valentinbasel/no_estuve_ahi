#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###############################################################################
# 
# Copyright © 2021 Valentin Basel <valentinbasel@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################


import numpy as np
import cv2
from multiprocessing import Process
import time

def local_video(): 
    import numpy as np
    import cv2
    path = r"video/v1.mp4"
    cap = cv2.VideoCapture(path)
    cv2.namedWindow("frame2", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("frame2",
                          cv2.WND_PROP_FULLSCREEN,
                          cv2.WINDOW_FULLSCREEN)
    p=1
    import socket
    while(True):
        mySocket = socket.socket(family=socket.AF_INET,
                                 type=socket.SOCK_STREAM,
                                 proto=0,
                                 fileno=None)
        hostname = 'localhost'
        portno = 9999
        mySocket.connect((hostname, portno))
        msg = "estado"
        mySocket.send(msg.encode())
        msg_in = mySocket.recv(1024).decode()
        p=int(msg_in)
        mySocket.close()
        ret, frame = cap.read()
        key = cv2.waitKey(p)
        if ret == True:
            cv2.imshow('frame2',frame)
        if key == 113:
            break
    cap.release()
    cv2.destroyAllWindows()


def local_video2(): 
    import numpy as np
    import cv2
    path = r"video/v2.mp4"
    cap = cv2.VideoCapture(path)
    cv2.namedWindow("frame2", cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty("frame2",cv2.WND_PROP_FULLSCREEN,cv2.WINDOW_FULLSCREEN)
    p=1
    import socket
    while(True):
        mySocket = socket.socket(family=socket.AF_INET,
                                 type=socket.SOCK_STREAM,
                                 proto=0,
                                 fileno=None)
        hostname = 'localhost'
        portno = 9999
        mySocket.connect((hostname, portno))
        msg = "estado"
        mySocket.send(msg.encode())
        msg_in = mySocket.recv(1024).decode() 
        p=int(msg_in)
        mySocket.close()
        ret, frame = cap.read()
        key = cv2.waitKey(p)
        if ret == True:
            cv2.imshow('frame2',frame)
        if key == 113:
            break
    cap.release()
    cv2.destroyAllWindows()

def servidor():
    """TODO: Docstring for servidor.
    :returns: TODO

    """
    import socket
    mySocket = socket.socket(family=socket.AF_INET,
                             type=socket.SOCK_STREAM,
                             proto=0,
                             fileno=None)
    print("Inicio el servidor")
    hostname = 'localhost'
    portno = 9999
    mySocket.bind((hostname, portno))
    mySocket.listen(5)
    p = 1
    while True:
        client, client_addr = mySocket.accept()
        msg = client.recv(1024).decode()
        str_val = str(p)
        c = str_val.encode()
        client.send(c)
        if msg.isdecimal():
            p = int(msg)*10
        if msg == "salir":
            break
            client.close()
        client.close()


if __name__ == '__main__':
    s1= Process(target = servidor)
    s1.start()
    time.sleep(1)
    p1= Process(target = local_video)
    p2= Process(target = local_video2)
    p1.start()
    p2.start()


